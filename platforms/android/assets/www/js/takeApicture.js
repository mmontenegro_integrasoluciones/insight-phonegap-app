var desde;
var usuario;
var pictureSource;   // Origen de la imagen
var destinationType; // Formato del valor retornado
var base64="";
var address; // Ubicación actual
var latitude; // Coordenadas a ser señaladas en el mapa
var longitude; // Coordenadas a ser señaladas en el mapa
var zoom = "17"; // Zoom para el mapa
var maptype = ""; // Tipo del mapa

var app = {
	// Application Constructor
	initialize: function(origen) {
		desde = origen;
		// Espera a que PhoneGap conecte con el dispositivo.
		document.addEventListener('deviceready', this.onDeviceReady, false);
	},

	// PhoneGap esta listo para usarse!
	onDeviceReady:function() {
		pictureSource=navigator.camera.PictureSourceType;
		destinationType=navigator.camera.DestinationType;
		if (desde == "usuario01" || desde == "usuario02") {
			getCurrentLocation();
		}
	},

	// Un botón llamara a esta función
	capturePhoto: function () {
		// Toma la imagen y la retorna como una string codificada en base64
		navigator.camera.getPicture(this.onPhotoDataSuccess, this.onFail, {quality: 50,
		destinationType: destinationType.DATA_URL,
		encodingType: Camera.EncodingType.PNG,
		correctOrientation:true});
	},

	// Llamada cuando la foto se retorna sin problemas
	onPhotoDataSuccess: function(imageData) {

		// Obtiene el elemento HTML de la imagen

		var largeImage = document.getElementById('largeImage');

		// Revela el elemento de la imagen

		largeImage.style.display = 'block';

		// Muestra la foto capturada
		// Se usan reglas CSS para dimensionar la imagen

		largeImage.src = "data:image/jpeg;base64," + imageData;
		base64 = imageData;
	},

	// Llamado cuando algo malo ocurre
	onFail:function (message) {
	alert('Ocurrió un error: ' + message);
	},

	entrar:function () {
		usuario = document.getElementById('codigo').value;
		if (usuario == 02) {
			//add
  		localStorage.setItem("usuario", 02);

			window.location.href="usuario02.html";
		} else {
			//add
  		localStorage.setItem("usuario", 01);

			window.location.href="usuario01.html";
		}
	},

	sendMail:function () {
		usuario = localStorage.getItem("usuario");
		if (usuario == 01) {
			var radios = document.getElementsByName('maptype');
			for (var i = 0, length = radios.length; i < length; i++) {
    			if (radios[i].checked) {
        			// do whatever you want with the checked radio
        			maptype = radios[i].value;

        			// only one radio can be logically checked, don't check the rest
        			break;
    			}
			}

			if (!base64) {
				alert("Debe tomar una fotografía para que el mensaje sea enviado");
			} else if (!latitude || !longitude || !address) {
				alert("Debe salir de la aplicación, encender su gps y volver a ingresar");
			} else if (!maptype) {
				alert("Debe seleccionar el tipo de mapa que se visualizará el mensaje");
			} else if (!document.getElementById('email').value ||
						!document.getElementById('subject').value) {
				alert("Debe completar los campos correo y asunto para que el mensaje sea enviado");
			} else {
				var email_array = document.getElementById('email').value.split(';');
				var data_mail = {address:address, email:email_array,
				subject:document.getElementById('subject').value,
				message:document.getElementById('message').value};

				// Sending and receiving data in JSON format using POST method (Email Request)
				xhr = new XMLHttpRequest();
				var url = "http://mailer.integrasoluciones.cl/";
				xhr.open("POST", url, false);
				xhr.setRequestHeader("Content-Type", "application/json");
				xhr.onreadystatechange = function () {
    			//if (xhr.readyState == 4 && xhr.status == 200) {
        			//var json = JSON.parse(xhr.responseText);
							//console.dir(xhr.responseText);
    			//}
				}

				var data = JSON.stringify({ "from_email": "servicios@cepati.com",
										"to_emails": data_mail.email, "subject": data_mail.subject,
										"template": "acems", "context": {"title": data_mail.subject,
        						"message": data_mail.message, "address": data_mail.address,
										"latitude": latitude, "longitude": longitude, "maptype": maptype,
										"zoom": zoom}, "attachments": [{"content_id": "image", "content_type": "image/jpeg",
										"content_binary": base64 }] });
				xhr.send(data);

				// Sending and receiving data in JSON format using POST method (News Request)
				xhr_news = new XMLHttpRequest();
				var url_news = "http://is-forwarding-testing.azurewebsites.net/news";
				xhr_news.open("POST", url_news, true);
				xhr_news.setRequestHeader("Content-Type", "application/json");
				//xhr_news.onreadystatechange = function () {
    				//if (xhr_news.readyState == 4 && xhr_news.status == 200) {
        				//var json_news = JSON.parse(xhr_news.responseText);
								//console.dir(xhr_news.responseText);
    				//}
				//}
				var data_news = JSON.stringify({"latitude": latitude, "longitude": longitude,"address":data_mail.address,
				"email":data_mail.email, "subject":data_mail.subject, "message":data_mail.message, "img":base64,
				"zoom":zoom, "maptype":maptype});
				xhr_news.send(data_news);

				alert("Su mensaje ha sido enviado a la dirección de correo electrónico: " + data_mail.email);
			}
		} else {
    		maptype = "roadmap";
			if (!base64) {
				alert("Debe tomar una fotografía para que el mensaje sea enviado");
			} else if (!latitude || !longitude || !address) {
				alert("Debe salir de la aplicación, encender su gps y volver a ingresar");
			} else if (!document.getElementById('subject').value) {
				alert("Debe completar el campo de asunto para que el mensaje sea enviado");
			} else {
				var email_array = "marimontenegro10@gmail.com";
				var data_mail = {address:address, email:email_array,
				subject:document.getElementById('subject').value,
				message:document.getElementById('message').value};

				// Sending and receiving data in JSON format using POST method (Email Request)
				xhr = new XMLHttpRequest();
				var url = "http://mailer.integrasoluciones.cl/";
				xhr.open("POST", url, false);
				xhr.setRequestHeader("Content-Type", "application/json");
				xhr.onreadystatechange = function () {
    			//if (xhr.readyState == 4 && xhr.status == 200) {
						//console.dir(xhr.responseText);
        		//var json = JSON.parse(xhr.responseText);
    			//}
				}

				var data = JSON.stringify({ "from_email": "servicios@cepati.com",
										"to_emails": data_mail.email, "subject": data_mail.subject,
										"template": "acems", "context": {"title": data_mail.subject,
        						"message": data_mail.message, "address": data_mail.address,
										"latitude": latitude, "longitude": longitude, "maptype": maptype,
										"zoom": zoom}, "attachments": [{"content_id": "image", "content_type": "image/jpeg",
										"content_binary": base64 }] });
				xhr.send(data);

				// Sending and receiving data in JSON format using POST method (News Request)
				xhr_news = new XMLHttpRequest();
				var url_news = "http://is-forwarding-testing.azurewebsites.net/news";
				xhr_news.open("POST", url_news, true);
				xhr_news.setRequestHeader("Content-Type", "application/json");
				//xhr_news.onreadystatechange = function () {
    				//if (xhr_news.readyState == 4 && xhr_news.status == 200) {
        				//var json_news = JSON.parse(xhr_news.responseText);
								//console.dir(xhr_news.responseText);
    				//}
				//}
				var data_news = JSON.stringify({"latitude": latitude, "longitude": longitude,"address":data_mail.address,
				"email":data_mail.email, "subject":data_mail.subject, "message":data_mail.message, "img":base64,
				"zoom":zoom, "maptype":maptype});
				xhr_news.send(data_news);

				alert("Su mensaje ha sido enviado a la dirección de correo electrónico: " + data_mail.email);
			}
		}
	}
};

function getBase64Image(img) {
  	var canvas = document.createElement("canvas");
  	canvas.width = img.width;
  	canvas.height = img.height;
  	var ctx = canvas.getContext("2d");
  	ctx.drawImage(img, 0, 0);
	var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpeg);base64,/, "");
}

function getCurrentLocation () {
	if (navigator.geolocation) {
        //returns current GPS position
    	navigator.geolocation.getCurrentPosition(locationSuccess, locationError, {enableHighAccuracy: true, timeout: 60000});
    	function locationSuccess(position) {
    		var currentPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      		latitude = position.coords.latitude;
      		longitude = position.coords.longitude;
      		getAddress(currentPosition);   // Pass the latitude and longitude to get address.
		}

		function locationError(error) {
			alert("Encienda el GPS y los datos móviles, de lo contrario su mensaje no podrá ser enviado");
    	//alert('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n');
		 //getCurrentLocation();
		}

		function getAddress(currentPosition) {
			var reverseGeocoder = new google.maps.Geocoder();
    		reverseGeocoder.geocode({'location': currentPosition}, function(results, status) {

            	if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                    	alert('La evidencia fue tomada en: ' + results[0].formatted_address);
                    	document.getElementById('currentPosition').innerHTML = results[0].formatted_address;
                    	address = results[0].formatted_address;
                    }
            		else {
                    	alert('Unable to detect your address.');
                    }
        		} else {
            	 alert("Geocode was not successful for the following reason: " + status);
        		}
    		});
		}
    } else {
        // tell the user if a browser doesn't support this amazing API
        alert("Your browser does not support the Geolocation API!");
    }
}
